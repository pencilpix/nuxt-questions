import createService from './service'
import * as types from './types'

export default function createActions(ctx) {
  const _api = createService(ctx)

  return {
    /**
     * save questions to the state
     * @param {Store} store vuex store
     * @param {Function} store.commit vuex commit mutation
     * @param {Array<Object>} questions list of questions
     * @returns {Promise}
     */
    loadQuestions({ commit }, questions) {
      commit(types.LOAD_QUESTIONS, questions)
      return Promise.resolve()
    },


    /**
     * save a question to the list
     * @param {Store} store vuex store
     * @param {Function} store.commit vuex commit mutation
     * @param {Object} question content of the question
     * @returns {Promise}
     */
    loadQuestion({ commit }, question) {
      commit(types.LOAD_QUESTION, question)
      return Promise.resolve()
    },


    /**
     * fetch post via id
     * @param {Store} store vuex store
     * @param {Function} store.commit vuex commit mutation
     * @param {Object} store.dispatch vuex dispatch action
     * @param {Object} options
     * @param {Number} options.id question id
     * @param {Boolean} options.update if its already on list, update or not.
     * @returns {Promise}
     */
    fetchQuestion({ dispatch, state }, { id, update }) {
      const isQuestion = state.list[id]
      if (isQuestion && !update) return Promise.resolve()

      return _api.fetchQuestion(id)
        .then((res) => dispatch('loadQuestion', res.content))
    },
  }
}
