import createActions from '<%= options.storePath %>/actions.js'
import mutations from '<%= options.storePath %>/mutations.js'
import getters from '<%= options.storePath %>/getters.js'

function createQuestionsModule(ctx) {
  return {
    namespaced: true,
    state: {
      list: {
        // ['postId']: { /* post */ }
      },

      slugList: {
        // 'post-slug': 'postId as a number'
      },
      singleId: null, // 'postId as a number',
      loading: false, // loading state of the single post.
    },

    actions: createActions(ctx),
    getters,
    mutations,
  }
}



export default function QuestionsModuleStore(ctx) {
  const { store } = ctx
  const opts = {}

  if (process.client) opts.preserveState = true

  store.registerModule('$questions', createQuestionsModule(ctx), opts)
}
