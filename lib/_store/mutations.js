import Vue from 'vue'
import * as types from './types'

export default {
  /**
   * save list of questions in the state
   * @param {Object} state vuex store local state
   * @param {Array<Object>} payload list of questions
   */
  [types.LOAD_QUESTIONS](state, payload) {
    const questions = payload || []
    questions.forEach(question => {
      Vue.set(state.list, `${question.id}`, question)
      Vue.set(state.slugList, `${question.slug}`, `${question.id}`)
    })
  },


  /**
   * save a question to the list
   * @param {Object} state vuex store local state
   * @param {Object} payload question content.
   */
  [types.LOAD_QUESTION](state, payload) {
    Vue.set(state.list, `${payload.id}`, payload)
  },
}
