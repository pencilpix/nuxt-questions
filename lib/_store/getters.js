export default {
  /**
   * get list of all questions registered to the state
   * @param {Object} state vuex module local state
   * @return {Array<Object>}
   */
  questions: state => state.list,


  /**
   * get map of slug to question id
   * @param {Object} state vuex module local state
   * @return {Object}
   */
  questionsSlugs: state => state.slugList,


  /**
   * get question by id
   * @param {Object} state vuex module local state
   * @param {Object} getters vuex module local getters
   * @param {Object} getters.questions get questions list
   * @return {(id: Number) => Object}
   */
  questionById: (state, { questions }) => id => questions[id],


  /**
   * get question by slug
   * @param {Object} state vuex module local state
   * @param {Object} getters vuex module local getters
   * @param {Object} getters.questions get questions list
   * @param {Object} getters.questionsSlugs get map of questions slugs to it's ids
   * @return {(slug: String) => Object}
   */
  questionBySlug: (state, { questions, questionsSlugs }) => slug => questions[questionsSlugs[slug]],


  /**
   * get question loading status.
   * @param {Object} state vuex module local state
   * @return {Boolean}
   */
  questionLoading: state => state.loading,
}
