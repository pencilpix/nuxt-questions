export default function createService(ctx) {
  const app = ctx.app

  return {
    get http() {
      return app.$axios
    },

    /**
     * fetch question from service via id
     * @param {Number} id question id
     * @returns {Promise}
     */
    fetchQuestion(id) {
      /**
       * TODO: (@pencilpix) api endpoints must be customized
       * by options. and must not be hard coded
       */
      console.log('fetching question', id)
      return this.http.$get(`/api/questions/${id}`)
    },
  }
}
