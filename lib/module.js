const { resolve } = require('path')

const DEFAULTS = {
}

module.exports = function QuestionsModule(moduleOptions) {
  const options = Object.assign({}, DEFAULTS, moduleOptions, this.options.questions)

  options.storePath = resolve(__dirname, './_store')

  // require comments module
  // since it's main dependency to the questions module.
  this.requireModule('@pencilpix/nuxt-answers')


  // provide posts store
  this.addPlugin({
    src: resolve(__dirname, './_store/index.js'),
    fileName: 'questions.store.js',
    options
  })
}
module.exports.meta = require(resolve(__dirname, '../package.json'))

