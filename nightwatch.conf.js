const { resolve } = require('path')
const BINPATH = resolve(__dirname, './node_modules/nightwatch/bin/')
const SCREENSHOT = resolve(__dirname, './tests/e2e/screenshots')

module.exports = {
  src_folders: ['test/e2e'],
  output_folder: resolve(__dirname, './test/e2e/reports'),
  selenium: {
    start_process: true,
    server_path: BINPATH + '/selenium.jar',
    host: '127.0.0.1',
    port: 4444,
    cli_args: {
      'webdriver.chrome.driver': BINPATH + '/chromedriver'
    }
  },


  test_settings: {
    default: {
      screenshots: {
        enabled: true,
        path: SCREENSHOT
      },

      globals: {
        waitForConditionTimeout: 10000,
        asyncHookTimeout: 60000
      },

      desiredCapabilities: {
        browserName: 'chrome'
      }
    },

    chrome: {
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true
      }
    }
  }
}


/**
 * selenium-download does exactly what it's name suggests;
 * downloads (or updates) the version of Selenium (& chromedriver)
 * on your localhost where it will be used by Nightwatch.
 * The following code checks for the existence of `selenium.jar`
 * before trying to run our tests.
 */

require('fs').stat(BINPATH + '/selenium.jar', function (err, stat) { // got it?
  if (err || !stat || stat.size < 1) {
    require('selenium-download').ensure(BINPATH, function (error) {
      if (error) throw new Error(error) // no point continuing so exit!
      console.log('✔ Selenium & Chromedriver downloaded to:', BINPATH)
    })
  }
})

